const books = require('../models/books-model');

const getAllBooks = async (offset) => {
  const dbBooks = await books.getAllBooks(offset);
  return dbBooks;
};

const createBooks = async (book) => {
  const dbBooks = await books.createBooks(book);
    return dbBooks;
};

const findBook = async (id) => {
  const dbBooks = await books.findBook(id);
    return dbBooks;
};

const updateBook = async (id, body) => {
  const dbBooks = await books.findBooks(id);
  if (!dbBooks) {
    return {
        message: 'book not found!',
    };
   }
   return books.updateBooks(id, body);
};

const removeBook = async (id) => {
  const dbBooks = await books.findBook(id);
  if (!dbBooks) {
    return {
        message: 'book not found!',
    };
   }
  await books.removeBook(id);

  return 'removed with sucess';
};

module.exports = {
  getAllBooks, createBooks, findBook, updateBook, removeBook,
};