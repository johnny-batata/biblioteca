const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const PORT = process.env.PORT || 3001;
const app = express();

app.use(cors());

app.use(bodyParser.json());

const books = require('./controllers/books-controller');

app.delete('/books/:id', books.removeBook);
app.put('/books/:id', books.updateBook);
app.post('/books', books.createBook);
app.get('/books/:offset', books.getAllBooks);
app.get('/books/book/:id', books.findBook);

app.listen(PORT, () => console.log(`conectado na porta ${PORT}`));
