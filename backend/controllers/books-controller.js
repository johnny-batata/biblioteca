const books = require('../services/books-services');

const createBook = async (req, res) => {
  const book = await books.createBooks(req.body);
  return res.status(201).json(book);
};

const getAllBooks = async (req, res) => {
  const { offset } = req.params;

  const book = await books.getAllBooks(offset);
  return res.status(200).json({ data: book.response, totalLength: book.responseLength });
};

const findBook = async (req, res) => {
  const { id } = req.params;
  const book = await books.findBook(id);
  if (book.message) return res.status(404).json({ data: book });
  return res.status(200).json(book);
};

const updateBook = async (req, res) => {
  const { id } = req.params;
  
  const book = await books.updateBook(id, req.body);

  if (book.message) return res.status(401).json(book);

  return res.status(200).json(book);
};

const removeBook = async (req, res) => {
  const { id } = req.params;
  
  const book = await books.removeBook(id);
  
  if (book.message) return res.status(401).json(book);

  return res.status(204).json({ message: 'book"s delete was a success!!' });
};

module.exports = {
  createBook, removeBook, updateBook, findBook, getAllBooks,
};