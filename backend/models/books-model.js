const mongoConnection = require('./connection/mongoConnection');

const getAllBooks = async (offset) => {
  const db = await mongoConnection();

  const response = await db.collection('books').find({}).sort({ _id: 1 }).skip(Number(offset))
  .limit(40)
.toArray();

const responseLength = await db.collection('books').find({}).count();

return { response, responseLength };
};

const createBooks = async (body) => {
  const { title, isbn, pageCount, 
    publishedDate, thumbnailUrl, shortDescription, longDescription, status, authors, categories,
     _id } = body; 
  const db = await mongoConnection();

    return db.collection('books')
    .insertOne({ 
      title,
      isbn,
      pageCount,
      publishedDate,
      thumbnailUrl,
      shortDescription, 
      longDescription,
      status,
      authors,
      categories,
      _id, 
    });
};

const findBook = async (id) => {
  const db = await mongoConnection();

  return db.collection('books').findOne({ _id: Number(id) });
};

const updateBooks = async (id, body) => {
  const { 
    title, isbn, pageCount, 
    publishedDate, thumbnailUrl, shortDescription, longDescription, status, authors, categories, 
  } = body; 
  const db = await mongoConnection();

  return db
  .collection('books').updateOne({ _id: Number(id) }, {
    $set: { title,
      isbn,
      pageCount,
      publishedDate,
      thumbnailUrl,
      shortDescription, 
      longDescription,
      status,
      authors,
      categories },
  });
};

const removeBooks = async (id) => {
  const db = await mongoConnection();

  return db
  .collection('books').deleteOne({ _id: Number(id) });
};

module.exports = {
  getAllBooks, createBooks, findBook, updateBooks, removeBooks,
};