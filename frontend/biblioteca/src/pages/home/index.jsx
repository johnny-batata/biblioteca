import React, { useEffect, useState } from 'react';
import { getAllBooks } from '../../services/api';
import BookCards from './components/bookCards';
import FootbarButtons from './components/footbarButtons';
import * as S from './index.styles';

const Home = () => {
  const [books, setBook] = useState([]);
  const [length, setLength] = useState([]);

  const fetchBooks = async (offset = 1) => {
    const book = await getAllBooks(offset);
    console.log('books', book, 'fetchbooks');
    setLength(book.totalLength);
    setBook(book.data);
  };

  useEffect(() => {
    fetchBooks();
  }, []);

  return (
    <div>
      <S.Section>
        { books && books.map((el, index) => <BookCards key={ index } book={ el } />)}
      </S.Section>

      <footer>
        <FootbarButtons length={ length } func={ fetchBooks } />
      </footer>
    </div>
  );
};

export default Home;
