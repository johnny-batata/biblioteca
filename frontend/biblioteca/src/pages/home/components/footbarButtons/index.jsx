import React from 'react';

const FootbarButtons = ({ length, func }) => {
  const renderButtons = () => {
    const totalLength = [];
    for (let i = 1; (i * 40) <= (length + 40); i += 1) {
      totalLength.push(i);
    }
    return totalLength;
  };

  return (
    <div>
      {renderButtons().map((el, index) => (
        <button type="button" key={ index } onClick={ () => func(el) }>
          {el}
        </button>
      ))}
    </div>
  );
};
export default FootbarButtons;
