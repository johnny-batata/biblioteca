import styled from 'styled-components';

export const Section = styled.section`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  justify-content: space-between;
  width: 355px;
  /* padding-left: 6px;
  padding-right: 6px; */
  margin: 8px;
  align-items: center;

  img {
    width: 60%;
  }



`;
