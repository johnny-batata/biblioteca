import React from 'react';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import * as S from './index.styles';

const BookCards = ({ book }) => {
  const { title, thumbnailUrl, publishedDate, categories } = book;

  const history = useHistory();

  const renderCategories = () => categories.map((el) => `${el} `);
  return (
    <S.Section>
      <img src={ thumbnailUrl } alt={ title } />
      <div>
        <h3>
          {title}
        </h3>
        <p>
          lançado em:
          {' '}
          {moment(publishedDate).format('DD/MM/YYYY')}
        </p>
        <p>
          Categorias:
          {' '}
          {''}
          {renderCategories()}
        </p>
        <button
          type="button"
          onClick={ () => history.push(`/book-details/${title}`) }
        >
          Ver mais

        </button>
      </div>

    </S.Section>
  );
};

export default BookCards;
