import React from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';
import * as S from './index.styles';

const BookDetailsCard = ({ book }) => {
  const { authors, categories, isbn, longDescription, pageCount, publishedDate, status, thumbnailUrl, title, _id } = book;
  console.log('render book details');
  return (
    <S.Section>
      <div>
        <S.ImageDiv>
          <h1>{title}</h1>
          <img src={ thumbnailUrl } alt={ title } />
        </S.ImageDiv>
        <S.AuthorsDiv>
          <div>
            <h4>Atualizar Livro:</h4>
            <Link to={ `/book-details/edit/${title}` }>
              <button type="button">Atualizar livro</button>
            </Link>
            <h4>Autores:</h4>
            <p>
              { authors && authors.map((el) => `${el} `)}
            </p>
          </div>
        </S.AuthorsDiv>

        <S.ReleaseDateDiv>
          <div>
            <p>
              <h4>Lançado em:</h4>
              {moment(publishedDate).format('DD/MM/YYYY')}
            </p>
          </div>
          <div>
            <p>
              <h4>Isbna:</h4>
              {' '}
              {isbn}
              {' '}
            </p>
          </div>
          <div>
            <p>
              <h4>Status:</h4>
              {' '}
              {status}
              {' '}
            </p>
          </div>
          <div>

            <p>
              <h4>Nº de páginas:</h4>
              {' '}
              {pageCount}
              {' '}
            </p>
          </div>
        </S.ReleaseDateDiv>

        <S.DescriptionDiv>
          <p>
            <h4>Descrição:</h4>
            {' '}
            {longDescription}
            {' '}
          </p>
          <p>
            <h4>Categorias:</h4>
            {' '}
            {categories && categories.map((el) => `${el}  `)}
            {' '}
          </p>

        </S.DescriptionDiv>
      </div>

    </S.Section>
  );
};
export default BookDetailsCard;
