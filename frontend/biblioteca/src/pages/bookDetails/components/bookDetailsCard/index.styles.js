import styled from 'styled-components';

export const Section = styled.section`
  display: flex;
  width: 100%;
  height: 100%;
  flex-direction: column;
  background-image: url('https://media.istockphoto.com/vectors/seamless-background-with-books-vector-id929023224') ;


  div {
    width: 100%;
  }
  
`;

export const DescriptionDiv = styled.div`
  box-shadow: 0 0 1px 1px  #333;
  display: flex;
  width: 100%;
  height: 100%;
  flex-direction: column;
  background-color: #fff;
  color: #3e5481;
  font-size: 16px;

`;

export const ReleaseDateDiv = styled.div`
  box-shadow: 0 0 1px 1px  #333;
  display: flex;
  width: 100%;
  height: 100%;
  flex-direction: column;
  background-color: #fff;
  color: #3e5481;
  font-size: 16px;
`;

export const AuthorsDiv = styled.div`
    background-color: #fff;
    display: flex;
    flex-direction: column;
    border-top-left-radius: 32px;
    border-top-right-radius: 32px;
    color: #3e5481;
    font-size: 16px;
`;

export const ImageDiv = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    img {
      width: 300px;
      height: 300px;
    }
    
`;
