/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom';
import { getBookByTitle } from '../../services/api';
import BookDetailsCard from './components/bookDetailsCard';
import * as S from './index.styles';

const BookDetails = () => {
  const [book, setBook] = useState({});

  const params = useParams();
  const fetchBook = async () => {
    const response = await getBookByTitle(params.title);
    setBook(response.data);
  };

  useEffect(() => {
    fetchBook();
  }, []);

  return (
    <S.MainDiv>
      { book && <BookDetailsCard book={ book } /> }
    </S.MainDiv>
  );
};

export default BookDetails;
