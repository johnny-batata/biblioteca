import React from 'react';
import moment from 'moment';
import BookCards from '../../../home/components/bookCards';

const BookForm = ({ book, setBook }) => {
  const { authors, categories, isbn, longDescription, pageCount, publishedDate, status, thumbnailUrl, title, _id } = book;
  return (
    <form>
      <label htmlFor="title">
        Title:
        <input id="title" type="text" value={ title } name="title" onChange={ setBook } />
      </label>
      <label htmlFor="isbn">
        Isbn:
        <input id="isbn" type="text" value={ isbn } name="isbn" onChange={ setBook } />
      </label>
      <label htmlFor="pageCount">
        PageCount:
        <input id="pageCount" type="number" value={ pageCount } name="pageCount" onChange={ setBook } />
      </label>
      <label htmlFor="thumbnailUrl">
        thumbnailUrl:
        <input id="thumbnailUrl" type="text" value={ thumbnailUrl } name="thumbnailUrl" onChange={ setBook } />
      </label>
      <label htmlFor="publishedDate">
        publishedDate:
        <input id="publishedDate" type="text" name="publishedDate" value={ moment(publishedDate).format('DD/MM/YYYY') } onChange={ setBook } />
      </label>
      <label htmlFor="longDescription">
        longDescription:
        <textarea rows="10" cols="30" id="longDescription" name="longDescription" type="text" value={ longDescription } onChange={ setBook } />
      </label>
      <section>
        Categorias
        {
          categories.map((el, index) => (
            <label htmlFor={ `categories-${index}` } key={ index }>
              <input id={ `categories-${index}` } name="categories" type="text" value={ el } onChange={ setBook } />
            </label>
          ))
        }
      </section>
      <section>
        Autores
        { authors.map((el, index) => (
          <input key={ index } id={ el } name="authors" type="text" value={ el } onChange={ setBook } />
        )) }
      </section>
      <label htmlFor="status">
        status:
        <input id="status" type="text" value={ status } onChange={ setBook } />
      </label>
      <button type="button">Enviar</button>
    </form>
  );
};

export default BookForm;
