/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getBookByTitle } from '../../services/api';
import BookForm from './components/bookForm';

const defaultBook = {
  authors: [],
  categories: [],
  isbn: '',
  longDescription: '',
  pageCount: 0,
  shortDescription: '',
  status: '',
  thumbnailUrl: '',
  title: '',
};

const BookEdit = () => {
  const [book, setBook] = useState(defaultBook);
  const params = useParams();
  const fetchBook = async () => {
    const response = await getBookByTitle(params.title);
    setBook(response.data);
  };

  useEffect(() => {
    fetchBook();
  }, []);

  const handleChange = ({ target: { value, name, id } }) => {
    // console.log('handleChange ', value, name);
    // if (name === 'categories') {

    // }
    // if (name === 'authors') {

    // }
    // setBook({ ...book, [name]: value });
  };

  console.log('arrozao', book);
  return (
    <div>
      <p>ola</p>
      {book && <BookForm book={ book } setBook={ handleChange } />}

    </div>
  );
};

export default BookEdit;
