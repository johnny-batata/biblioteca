import axios from 'axios';

const getAllBooks = async (offset = 1) => {
  const response = await axios({
    method: 'get',
    url: `http://localhost:3001/books/${offset}`,
  })
    .then((data) => data)
    .catch((err) => err);

  return response.data;
};

const getBookByTitle = async (title) => {
  const response = axios({
    method: 'get',
    url: `http://localhost:3001/books/book/${title}`,
  })
    .then((data) => data)
    .catch((err) => err);

  return response;
};

export { getAllBooks, getBookByTitle };
