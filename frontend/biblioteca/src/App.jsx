import React from 'react';
import { Route } from 'react-router-dom';
import Home from './pages/home';
import BookDetails from './pages/bookDetails';
import BookEdit from './pages/bookEdit';

function App() {
  return (
    <div>
      <Route exact path="/book-details/:title" component={ BookDetails } />
      <Route exact path="/book-details/edit/:title/" component={ BookEdit } />
      <Route exact path="/" component={ Home } />
    </div>
  );
}

export default App;
